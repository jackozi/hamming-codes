import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Jacko
 * Date: 23-9-13
 * Time: 16:56
 * To change this template use File | Settings | File Templates.
 */
public class ham {

    public static void main(String[] args){
        String hCode = "";
        ArrayList<String> hList = new ArrayList<String>();
        if(args.length != 3){
            System.out.println("Usage: ham <length> <distance> <random>.");
        }else{
            Long lng = Long.valueOf(args[0]);
            Long dst = Long.valueOf(args[1]);
            Long max = new Long(2);
            for(int i=1;i<lng;i++){
                max = max*2;
            }
            for(int i=0;i<lng;i++){
                 hCode += "0";
            }
            hList.add(hCode);
            if(Integer.valueOf(args[2])!=0){
                while(true){
                    hCode = generateRandom(lng);
                    if(fits(hCode, hList, dst)){
                        hList.add(hCode);
                        System.out.println(hCode);
                    }
               }
            }else{
            int runs = 1;
                while(runs<max){
                    hCode = generateNext(lng, runs);
                    if(fits(hCode, hList, dst)){
                        hList.add(hCode);
                        System.out.println(hCode);
                    }
                    runs++;
                }
            }
        }
    }

    private static String generateRandom(Long lng){
        String hCode = "";
        for(int i=0;i<lng;i++){
            int newChar = 1+(int)(Math.random()*2);
            newChar--;
            hCode += newChar;
        }
        return hCode;
    }

    private static String generateNext(long lng, long value) {
        String hCode = Long.toBinaryString(value);
        while(hCode.length() < lng){
            hCode = "0" + hCode;
        }
        return hCode;
    }

    private static boolean fits(String hCode, ArrayList<String> hList, Long dst){
        for(String hCodeToCheck : hList){
            int difChar = 0;
            for(int i=0;i<hCode.length();i++){
                if(hCode.charAt(hCode.length()-1-i) != hCodeToCheck.charAt(hCode.length()-1-i)){
                    difChar++;
                }
            }
            if(difChar < dst){
                return false;
            }
        }
        return true;
    }
}
